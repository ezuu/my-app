import { Component, OnInit } from '@angular/core';
import { log } from 'util'; 
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  name: string;
  age: number;
  email: string;
  address: Address;
  hobbies: string[];
  posts: Post[];
  isEdit: boolean = false;

  constructor(private dataService: DataService) { 
    console.log('Constructor ran...');
  }

  ngOnInit() {
     console.log('ngOnInit ran...');
     this.name = 'John Doe';
     this.age = 30;
     this.email = 'ezauu@gmail.com'
     this.address = {
       street: 'Bukit Jalil',
       city: 'Kuala Lumpur',
       state: 'Malaysia'
     }
     this.hobbies = ['write code', 'watch movies', 'listen to music'];

     this.dataService.getPosts().subscribe((posts) => {
       //console.log(posts);
       this.posts = posts;
     });
  }

  onClick() {
    //console.log("Hello");
    this.name = 'Wassup Buddy!';
  }

  addHobby(hobby) {
    console.log(hobby);
    this.hobbies.push(hobby);
    return false;
  }

  deleteHobby(hobby) {
    console.log(hobby);

    for (let i = 0; i < this.hobbies.length; i++) {
      if (this.hobbies[i] == hobby) {
        this.hobbies.splice(i, 1);
      }
    }
  }

  toggleEdit() {
    this.isEdit = !this.isEdit;
  }

}


interface Address {
  street: string,
  city: string,
  state: string
}

interface Post {
  id: number,
  title: string,
  body: string,
  userId: number
}